﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clothbazar.web.Controllers
{
    public class SharedController : Controller
    {
        // GET: Shared
        public JsonResult UploadImage()
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            try
            {
                var file = Request.Files[0];
                var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Images"), fileName);
                file.SaveAs(path);

                // var newImage = new Image() { Name = fileName };
                //if(ImageService.Instance.SaveNewImage(newImage))
                //{
                //    result.Data = new { Success = true, Image = fileName, File = newImage.ID, ImageUrl = string.Format("{0}{1}") };
                //}
                //else
                //{
                //    result.Data = new { Success = false, Message = new HttpStatusCodeResult(500) };
                //}
                result.Data = new { Success = true, Image = fileName,ImageUrl =string.Format("/Content/Images/{0}",fileName)};
            }
            catch (Exception ex)
            {

                result.Data = new { Success = false, Message = ex.Message };
            }
            return result;
        }
    }
}