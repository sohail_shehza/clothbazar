﻿using clothbazar.web.ViewModels;
using ClothBazar.Entities;
using ClothBazar.Services;
using System.Linq;
using System.Web.Mvc;

namespace clothbazar.web.Controllers
{
    public class ProductController : Controller
    {
        ProductsService productsService = new ProductsService();
        CategoriesService categoryservice = new CategoriesService();
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        

        public ActionResult ProductTable(string search)
        {
            var products = productsService.GetProduct();
            if(!string.IsNullOrEmpty(search))
            {
                products = products.Where(p =>p.Name!=null && p.Name.ToLower().Contains(search.ToLower())).ToList();
            }
            
            return PartialView(products);
        }


        [HttpGet]
        public ActionResult Create()
        {

          
            var categories = categoryservice.GetCategory();
            return PartialView(categories);
        }
        [HttpPost]
        public ActionResult Create(NewCategoryViewModel model)
        {
            var newProduct = new Product();
            newProduct.Name = model.Name;
            newProduct.Description = model.Description;
            newProduct.Price = model.Price;
           // newProduct.CategoryID = model.CategoryID;
            newProduct.Category = categoryservice.GetCategory( model.CategoryID);
            productsService.SaveProduct(newProduct);
            return RedirectToAction("ProductTable");
        }

        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var product = productsService.GetProduct(ID);
            return PartialView(product);
        }
        [HttpPost]
        public ActionResult Edit(Product product)
        {
            productsService.UpdateProduct(product);
            return RedirectToAction("ProductTable");
        }

        [HttpPost]
        public ActionResult Delete(int ID)
        {
            productsService.DeleteProduct(ID);
            return RedirectToAction("ProductTable");
        }
    }
}