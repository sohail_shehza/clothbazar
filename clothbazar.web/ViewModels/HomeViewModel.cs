﻿using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clothbazar.web.ViewModels
{
    public class HomeViewModel
    {
        public List <Category> Categories{ get; set; }
    }
}