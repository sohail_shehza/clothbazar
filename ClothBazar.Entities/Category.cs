﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entities
{
    public class Category:BasicEntities
    {

        public string ImageURL { get; set; }
        List<Product> products { get; set; }

    }
}
